using System;
using System.Threading.Tasks;
using RelatoriosAPI.DTO;
using RelatoriosAPI.DTOs;
using RelatoriosAPI.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace RelatoriosAPI.Controllers {

    [Route("api/[controller]"), ApiController] public class FiltroController : ControllerBase {

        private readonly IFiltroAppService appService;
        private readonly ILogger logger;
        private IMapper mapper;

        public FiltroController(IFiltroAppService appService, ILogger<FiltroController> logger, IMapper mapper) {
            this.appService = appService;
            this.logger = logger;
            this.mapper = mapper;
        }

        [HttpGet, AllowAnonymous, Route("v1/filtrarpordata/")] public async Task<ActionResult> FiltrarPorData(DateTime dataInicial, DateTime dataFinal) {
            var retorno = new ResponseDTO<IEnumerable<ItemDTO>>();
            try {
                retorno.Data = await appService.FiltrarPorData(dataInicial, dataFinal);
                retorno.Status = retorno.Data != null;
                retorno.Message = retorno.Status ? "OK" : "Não foi possível buscar os dados";
                retorno.ExceptionMessage = retorno.Status ? null : "Não foi possível buscar os dados";
                return Ok(retorno);
            }
            catch (Exception ex) {
                logger.LogError(ex, "[FiltroController.FiltrarPorData] - Erro ao buscar os dados." + ex.Message + " | StackTrace = " + ex.StackTrace, null);
                retorno.Status = false;
                retorno.ExceptionMessage = ex.Message;
                return BadRequest(retorno);
            }
        } 

    }
}
