﻿using RelatoriosAPI.AppService;
using RelatoriosAPI.Interfaces;
using RelatoriosAPI.Others;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace RelatoriosAPI.IoC {

    public static class Injector {

        public static void RegisterServices(IServiceCollection services, IConfiguration configuration) {
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDistributedMemoryCache();

            services.AddScoped<IFiltroAppService, FiltroAppService>();

            var mappingConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new RelatoriosProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new OpenApiInfo {
                    Version = "v1",
                    Title = "RelatoriosAPI",
                    Description = "Api filtro de itens",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact {
                        Name = "eu",
                        Email = string.Empty,
                        Url = new Uri("https://twitter.com/spboyer"),
                    },
                    License = new OpenApiLicense {
                        Name = "nome da licença",
                        Url = new Uri("https://example.com/license"),
                    }
                });
                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

        }

    }
}

