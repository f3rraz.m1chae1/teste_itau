﻿
namespace RelatoriosAPI.DTOs {
    public class ResponseDTO<T> {

        public T Data { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }

    }

}
