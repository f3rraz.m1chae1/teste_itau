﻿using RelatoriosAPI.DTO;
using RelatoriosAPI.Entities;
using AutoMapper;

namespace RelatoriosAPI.Others {

    public class RelatoriosProfile : Profile {

        public RelatoriosProfile() {
            CreateMap<ItemDTO, ItemEntity>();
            CreateMap<ItemEntity, ItemDTO>();
        }

    }
}
