using AutoMapper;
using RelatoriosAPI.Interfaces;
using RelatoriosAPI.DTO;
using RelatoriosAPI.DAOs;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System;

namespace RelatoriosAPI.AppService {
    public class FiltroAppService : IFiltroAppService {
        private IMapper mapper;

        private FiltroDAO filtroDAO;

        public FiltroAppService(IMapper mapper_, IConfiguration config) {
            mapper = mapper_;
            this.filtroDAO = new FiltroDAO(config);
        }

        public async Task<IEnumerable<ItemDTO>> FiltrarPorData(DateTime dataInicial, DateTime dataFinal) { 
            return await filtroDAO.FiltrarPorData(dataInicial, dataFinal); 
        }  

    }

}

