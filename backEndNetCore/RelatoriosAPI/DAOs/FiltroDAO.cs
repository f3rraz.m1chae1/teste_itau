using RelatoriosAPI.Entities;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using RelatoriosAPI.DTO;
using System.Collections.Generic;

namespace RelatoriosAPI.DAOs {
    public class FiltroDAO {

        private readonly MySqlConnection mySqlConn;

        public FiltroDAO(IConfiguration config) {
            string strConnection = config.GetValue<string>("Connection:ConnectionString");
            mySqlConn = new MySqlConnection(strConnection);
            mySqlConn.Open();
        }

        public Task<IEnumerable<ItemDTO>> FiltrarPorData(DateTime dataInicial, DateTime dataFinal) {
            try {
                return Task<IEnumerable<ItemDTO>>.Factory.StartNew(() => {
                    using (MySqlCommand r = new MySqlCommand($"SELECT * FROM ITENS WHERE (DAY >= '{dataInicial.ToString("yyyy-MM-dd")}') AND (DAY <= '{dataFinal.ToString("yyyy-MM-dd")}')", mySqlConn)) {
                        List<ItemDTO> retorno = new List<ItemDTO>();
                        MySqlDataReader reader = r.ExecuteReader();
                        while (reader.Read()) {
                            retorno.Add(new ItemDTO() {
                                Id = Convert.ToInt32(reader["ID"]), 
                                Value = Convert.ToInt32(reader["VALUE"]),
                                Description = Convert.ToString(reader["DESCRIPTION"]),
                                Tag = Convert.ToInt32(reader["TAG"]),
                                Date = Convert.ToDateTime(reader["DAY"])
                            });
                        }
                        return retorno;
                    }
                });
            }
            catch (Exception e) {
                throw e;
            }
        }

    }
}

