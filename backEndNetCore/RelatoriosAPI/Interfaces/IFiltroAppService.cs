using RelatoriosAPI.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RelatoriosAPI.Interfaces {
    public interface IFiltroAppService {
        
        Task<IEnumerable<ItemDTO>> FiltrarPorData(DateTime dataInicial, DateTime dataFinal);
		
    }
}
