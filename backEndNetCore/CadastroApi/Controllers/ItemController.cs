using System;
using System.Threading.Tasks;
using CadastroApi.DTO;
using CadastroApi.DTOs;
using CadastroApi.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CadastroApi.Controllers {

    [Route("api/[controller]"), ApiController] public class ItemController : ControllerBase {

        private readonly IItemAppService appService;
        private readonly ILogger logger;
        private IMapper mapper;

        public ItemController(IItemAppService appService, ILogger<ItemController> logger, IMapper mapper) {
            this.appService = appService;
            this.logger = logger;
            this.mapper = mapper;
        }

        [HttpPost, AllowAnonymous, Route("v1/criar/")] public async Task<ActionResult> Criar(ItemDTO dto) {
            var retorno = new ResponseDTO<ItemDTO>();
            try {
                retorno.Data = await appService.Criar(dto);
                retorno.Status = retorno.Data != null;
                retorno.Message = retorno.Status ? "OK" : "Não foi possível criar o item";
                retorno.ExceptionMessage = retorno.Status ? null : "Não foi possível criar o item";
                return Ok(retorno);
            }
            catch (Exception ex) {
                logger.LogError(ex, "[ItemController.Criar] - Erro ao autenticar." + ex.Message + " | StackTrace = " + ex.StackTrace, null);
                retorno.Status = false;
                retorno.ExceptionMessage = ex.Message;
                return BadRequest(retorno);
            }
        } 

        [HttpPost, AllowAnonymous, Route("v1/alterar/")] public async Task<ActionResult> Alterar(ItemDTO dto) {
            var retorno = new ResponseDTO<ItemDTO>();
            try {
                retorno.Data = await appService.Alterar(dto);
                retorno.Status = retorno.Data != null;
                retorno.Message = retorno.Status ? "OK" : "Não foi possível alterar o item";
                retorno.ExceptionMessage = retorno.Status ? null : "Não foi possível alterar o item";
                return Ok(retorno);
            }
            catch (Exception ex) {
                logger.LogError(ex, "[ItemController.Alterar] - Erro ao alterar o item." + ex.Message + " | StackTrace = " + ex.StackTrace, null);
                retorno.Status = false;
                retorno.ExceptionMessage = ex.Message;
                return BadRequest(retorno);
            }
        }

        [HttpPost, AllowAnonymous, Route("v1/remover/")] public async Task<ActionResult> Remover(ItemDTO dto) {
            var retorno = new ResponseDTO<bool>();
            try {
                retorno.Data = await appService.Remover(dto);
                retorno.Status = retorno.Data;
                retorno.Message = retorno.Status ? "OK" : "Não foi possível remover o item";
                retorno.ExceptionMessage = retorno.Status ? null : "Não foi possível remover o item";
                return Ok(retorno);
            }
            catch (Exception ex) {
                logger.LogError(ex, "[ItemController.Remover] - Erro ao remover o item." + ex.Message + " | StackTrace = " + ex.StackTrace, null);
                retorno.Status = false;
                retorno.ExceptionMessage = ex.Message;
                return BadRequest(retorno);
            }
        }

    }
}
