
using System;

namespace CadastroApi.Entities {
	
    public class ItemEntity { 
        public int Id { get; set; }
        public double Value { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public int Tag { get; set; }
    }

}
