using CadastroApi.Entities;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;

namespace CadastroApi.DAOs {
    public class ItemDAO {

        private readonly MySqlConnection mySqlConn;

        public ItemDAO(IConfiguration config) {
            string strConnection = config.GetValue<string>("Connection:ConnectionString");
            mySqlConn = new MySqlConnection(strConnection);
            mySqlConn.Open();
        }

        public Task<ItemEntity> Criar(ItemEntity entity) {
            try {
                return Task<ItemEntity>.Factory.StartNew(() => {
                    ItemEntity retorno = new ItemEntity();
                    using (MySqlCommand c = new MySqlCommand($"INSERT INTO ITENS (TAG, VALUE, DESCRIPTION, DAY) VALUES ({entity.Tag}, {entity.Value}, '{entity.Description}', '{entity.Date.ToString("yyyy-MM-dd")}')", mySqlConn)) retorno.Id = c.ExecuteNonQuery();
                    using (MySqlCommand r = new MySqlCommand($"SELECT * FROM ITENS WHERE ID = LAST_INSERT_ID()", mySqlConn)) {
                        MySqlDataReader reader = r.ExecuteReader();
                        if (reader.Read()) {
                            retorno.Id = Convert.ToInt32(reader["ID"]);
                            retorno.Tag = Convert.ToInt32(reader["TAG"]);
                            retorno.Value = Convert.ToDouble(reader["VALUE"]);
                            retorno.Description = Convert.ToString(reader["DESCRIPTION"]);
                            retorno.Date = Convert.ToDateTime(reader["DAY"]);
                        }
                        return retorno;
                    }
                });
            }
            catch (Exception e) {
                throw e;
            }
        }

        public Task<bool> Remover(ItemEntity entity) {
            try {
                return Task<bool>.Factory.StartNew(() => {
                    using (MySqlCommand r = new MySqlCommand($"SELECT 1 FROM ITENS WHERE ID = {entity.Id}", mySqlConn)) {
                        MySqlDataReader reader = r.ExecuteReader();
                        if (!reader.Read()) throw new Exception($"N�o foi poss�vel remover, o item {entity.Id} n�o foi encontrado");
                    }
                    using (MySqlCommand c = new MySqlCommand($"DELETE FROM ITENS WHERE ID = {entity.Id}", mySqlConn)) {
                        c.ExecuteNonQuery();
                        return true;
                    }
                });
            }
            catch (Exception e) {
                throw e;
            }
        }

        public Task<ItemEntity> Alterar(ItemEntity entity) {
            try {
                return Task<ItemEntity>.Factory.StartNew(() => {
                    ItemEntity retorno = new ItemEntity();
                    using (MySqlCommand c = new MySqlCommand($"UPDATE ITENS SET TAG = {entity.Tag}, VALUE = {entity.Value}, DESCRIPTION = '{entity.Description}', DAY = '{entity.Date.ToString("yyyy-MM-dd")}' WHERE ID = {entity.Id}", mySqlConn)) retorno.Id = c.ExecuteNonQuery();
                    using (MySqlCommand r = new MySqlCommand($"SELECT * FROM ITENS WHERE ID = {entity.Id}", mySqlConn)) {
                        MySqlDataReader reader = r.ExecuteReader();
                        if (reader.Read()) {
                            retorno.Id = Convert.ToInt32(reader["ID"]);
                            retorno.Tag = Convert.ToInt32(reader["TAG"]);
                            retorno.Value = Convert.ToDouble(reader["VALUE"]);
                            retorno.Description = Convert.ToString(reader["DESCRIPTION"]);
                            retorno.Date = Convert.ToDateTime(reader["DAY"]);
                        }
                        return retorno;
                    }
                });
            }
            catch (Exception e) {
                throw e;
            }
        }

    }
}

