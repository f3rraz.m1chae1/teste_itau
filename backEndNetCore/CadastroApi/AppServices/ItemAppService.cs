using AutoMapper;
using CadastroApi.Interfaces;
using CadastroApi.DTO;
using CadastroApi.DAOs;
using CadastroApi.Entities;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace CadastroApi.AppService {
    public class ItemAppService : IItemAppService {
        private IMapper mapper;

        private ItemDAO itemDAO;

        public ItemAppService(IMapper mapper_, IConfiguration config) {
            mapper = mapper_;
            this.itemDAO = new ItemDAO(config);
        }

        public async Task<bool> Remover(ItemDTO dto) {
            ItemEntity entity = mapper.Map<ItemEntity>(dto);
            return await itemDAO.Remover(entity);
        }  

        public async Task<ItemDTO> Criar(ItemDTO dto) {
            ItemEntity entity = mapper.Map<ItemEntity>(dto);
            ItemEntity novoEntity = await itemDAO.Criar(entity);
            ItemDTO novoDto = mapper.Map<ItemDTO>(novoEntity);
            return novoDto;
        }

        public async Task<ItemDTO> Alterar(ItemDTO dto) {
            ItemEntity entity = mapper.Map<ItemEntity>(dto);
            ItemEntity novoEntity = await itemDAO.Alterar(entity);
            ItemDTO novoDto = mapper.Map<ItemDTO>(novoEntity);
            return novoDto;
        }

    }

}

