using CadastroApi.DTO;
using System.Threading.Tasks;

namespace CadastroApi.Interfaces {
    public interface IItemAppService {
        
        Task<bool> Remover(ItemDTO dto);
        Task<ItemDTO> Criar(ItemDTO dto);
        Task<ItemDTO> Alterar(ItemDTO dto);

    }
}
