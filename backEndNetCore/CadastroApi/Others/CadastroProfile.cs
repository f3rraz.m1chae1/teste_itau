﻿using CadastroApi.DTO;
using CadastroApi.Entities;
using AutoMapper;

namespace CadastroApi.Others {

    public class CadastroProfile : Profile {

        public CadastroProfile() {
            CreateMap<ItemDTO, ItemEntity>();
            CreateMap<ItemEntity, ItemDTO>();
        }

    }
}
