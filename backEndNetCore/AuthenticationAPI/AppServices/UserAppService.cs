using AutoMapper;
using AuthenticationAPI.Interfaces;
using AuthenticationAPI.DTO;
using AuthenticationAPI.DAOs;
using AuthenticationAPI.Entities;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace AuthenticationAPI.AppService {
    public class UserAppService : IUserAppService {
        private IMapper mapper;

        private UserDAO userDAO;

        public UserAppService(IMapper mapper_, IConfiguration config) {
            mapper = mapper_;
            this.userDAO = new UserDAO(config);
        }


        public async Task<UserDTO> Logar(UserDTO dto) { 
            UserEntity entity = mapper.Map<UserEntity>(dto); 
            UserEntity novoEntity = await userDAO.Logar(entity); 
            UserDTO novoDto = mapper.Map<UserDTO>(novoEntity); 
            return novoDto; 
        }  

        public async Task<UserDTO> Criar(UserDTO dto) {
            UserEntity entity = mapper.Map<UserEntity>(dto);
            UserEntity novoEntity = await userDAO.Criar(entity);
            UserDTO novoDto = mapper.Map<UserDTO>(novoEntity);
            return novoDto;
        }

    }

}

