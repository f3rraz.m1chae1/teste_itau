using System;
using System.Threading.Tasks;
using AuthenticationAPI.DTO;
using AuthenticationAPI.DTOs;
using AuthenticationAPI.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AuthenticationAPI.Controllers {

    [Route("api/[controller]"), ApiController] public class UserController : ControllerBase {

        private readonly IUserAppService appService;
        private readonly ILogger logger;
        private IMapper mapper;

        public UserController(IUserAppService appService, ILogger<UserController> logger, IMapper mapper) {
            this.appService = appService;
            this.logger = logger;
            this.mapper = mapper;
        }

        [HttpPost, AllowAnonymous, Route("v1/logar/")] public async Task<ActionResult> Logar(UserDTO dto) {
            var retorno = new ResponseDTO<UserDTO>();
            try {
                retorno.Data = await appService.Logar(dto);
                retorno.Status = retorno.Data != null;
                retorno.Message = retorno.Status ? "OK" : "Não foi possível autenticar";
                retorno.ExceptionMessage = retorno.Status ? null : "Não foi possível autenticar";
                return Ok(retorno);
            }
            catch (Exception ex) {
                logger.LogError(ex, "[UserController.Logar] - Erro ao autenticar." + ex.Message + " | StackTrace = " + ex.StackTrace, null);
                retorno.Status = false;
                retorno.ExceptionMessage = ex.Message;
                return BadRequest(retorno);
            }
        } 

        [HttpPost, AllowAnonymous, Route("v1/criar/")] public async Task<ActionResult> Criar(UserDTO dto) {
            var retorno = new ResponseDTO<UserDTO>();
            try {
                retorno.Data = await appService.Criar(dto);
                retorno.Status = retorno.Data != null;
                retorno.Message = retorno.Status ? "OK" : "Não foi possível criar o novo usuário";
                retorno.ExceptionMessage = retorno.Status ? null : "Não foi possível criar o novo usuário";
                return Ok(retorno);
            }
            catch (Exception ex) {
                logger.LogError(ex, "[UserController.Criar] - Erro ao criar o novo usuário." + ex.Message + " | StackTrace = " + ex.StackTrace, null);
                retorno.Status = false;
                retorno.ExceptionMessage = ex.Message;
                return BadRequest(retorno);
            }
        }


    }
}
