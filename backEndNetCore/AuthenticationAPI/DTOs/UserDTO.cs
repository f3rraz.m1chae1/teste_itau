
namespace AuthenticationAPI.DTO {
    public class UserDTO {
        public int Id { get; set; }
        public string User { get; set; }
        public string NickName { get; set; }
        public string PwHash { get; set; }
    }
}
