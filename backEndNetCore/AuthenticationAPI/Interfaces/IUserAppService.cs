using AuthenticationAPI.DTO;
using System.Threading.Tasks;

namespace AuthenticationAPI.Interfaces {
    public interface IUserAppService {
        
        Task<UserDTO> Logar(UserDTO dto);
        Task<UserDTO> Criar(UserDTO dto);
		
    }
}
