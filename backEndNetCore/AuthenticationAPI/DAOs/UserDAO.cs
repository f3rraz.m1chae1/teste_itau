using AuthenticationAPI.Entities;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;

namespace AuthenticationAPI.DAOs {
    public class UserDAO {

        private readonly MySqlConnection mySqlConn;

        public UserDAO(IConfiguration config) {
            string strConnection = config.GetValue<string>("Connection:ConnectionString");
            mySqlConn = new MySqlConnection(strConnection);
            mySqlConn.Open();
        }

        public Task<UserEntity> Logar(UserEntity entity) {
            try {
                return Task<UserEntity>.Factory.StartNew(() => {
                    using (MySqlCommand r = new MySqlCommand($"SELECT * FROM USERS WHERE USER = '{entity.User}'", mySqlConn)) {
                        MySqlDataReader reader = r.ExecuteReader();
                        if (reader.Read()) {
                            UserEntity user = new UserEntity();
                            user.Id = Convert.ToInt32(reader["ID"]);
                            user.NickName = Convert.ToString(reader["NICKNAME"]);
                            user.PwHash = Convert.ToString(reader["PWHASH"]);
                            user.User = Convert.ToString(reader["USER"]);
                            if (entity.PwHash == user.PwHash) return user; else throw new Exception("Senha incorreta");
                        }
                        else {
                            throw new Exception("Erro de conex�o");
                        }
                    }
                });
            }
            catch (Exception e) {
                throw e;
            }
        }

        public Task<UserEntity> Criar(UserEntity entity) {
            try {
                return Task<UserEntity>.Factory.StartNew(() => {
                    using (MySqlCommand r = new MySqlCommand($"SELECT * FROM USERS WHERE USER = '{entity.User}'", mySqlConn)) {
                        MySqlDataReader reader = r.ExecuteReader();
                        if (reader.Read()) throw new Exception($"J� existe outro usu�rio com o user '{entity.User}'");
                    }
                    using (MySqlCommand c = new MySqlCommand($"INSERT INTO USERS (NICKNAME, PWHASH, USER) VALUES ('', '{entity.PwHash}', '{entity.User}')", mySqlConn)) {
                        c.ExecuteNonQuery();
                        return new UserEntity() { User = entity.User };
                    }
                });
            }
            catch (Exception e) {
                throw e;
            }
        }
    }
}

