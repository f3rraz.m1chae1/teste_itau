﻿using AuthenticationAPI.DTO;
using AuthenticationAPI.Entities;
using AutoMapper;

namespace AuthenticationAPI.Others {

    public class AuthenticationProfile : Profile {

        public AuthenticationProfile() {
            CreateMap<UserDTO, UserEntity>();
            CreateMap<UserEntity, UserDTO>();
        }

    }
}
