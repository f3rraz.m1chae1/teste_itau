import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'date'}) export class PipeDate implements PipeTransform {

    transform(value:Date):string {
        const dt:Date = value;
        const mm = dt.getMonth() + 1;
        const dd = dt.getDate();
        return (dd < 10 ? "0" + dd : dd) + "/" + (mm < 10 ? "0" + mm : mm) + "/" + dt.getFullYear();
    }

}