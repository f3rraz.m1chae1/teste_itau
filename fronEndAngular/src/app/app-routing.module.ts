import { NgModule }             from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent }       from "./components/login/login.component";
import { CadastrarComponent }   from "./components/cadastrar/cadastrar.component";
import { RelatorioComponent }   from "./components/relatorio/relatorio.component";

const routes: Routes = [
    { path: "login"    , component: LoginComponent }, 
    { path: "cadastrar", component: CadastrarComponent }, 
    { path: "relatorio", component: RelatorioComponent }, 
    { path: ""         , redirectTo: "login", pathMatch: "full" }
];

@NgModule({imports: [RouterModule.forRoot(routes)], exports: [RouterModule]}) export class AppRoutingModule { }
