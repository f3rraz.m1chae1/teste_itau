import { BrowserModule }      from "@angular/platform-browser";
import { NgModule }           from "@angular/core";
import { FormsModule }        from "@angular/forms";
import { AppRoutingModule }   from "./app-routing.module";
import { AppComponent }       from "./app.component";
import { LoginComponent }     from "./components/login/login.component";
import { CadastrarComponent } from "./components/cadastrar/cadastrar.component";
import { RelatorioComponent } from './components/relatorio/relatorio.component';
import { SnackBarComponent }  from "./components/snack/snack-bar.component";
import { PipeDate }           from "./pipes/pipes.pipe";
import { HttpClientModule }   from "@angular/common/http";
import { CadastroService }    from "./services/cadastro.service";
import { RelatoriosService }  from "./services/relatorios.service";
import { AuthenticationService } from "./services/authentication.service";


@NgModule({
    declarations: [AppComponent, LoginComponent, CadastrarComponent, RelatorioComponent, SnackBarComponent, PipeDate],
    imports     : [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
    providers   : [AuthenticationService, CadastroService, RelatoriosService],
    bootstrap   : [AppComponent]
})
export class AppModule { }
