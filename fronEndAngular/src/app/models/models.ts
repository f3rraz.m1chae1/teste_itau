
export class User {
    public id:number;
    public user:string;
    public nickName:string;
    public pwHash:string;
}

export class Item {
    public id:number;
    public value:number;
    public description?:string;
    public date:Date;
    public tag?:number;
}
