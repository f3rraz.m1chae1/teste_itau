import { Component, OnInit } from "@angular/core";
import { Item }              from "src/app/models/models";
import { Router }            from "@angular/router";
import { RelatoriosService } from "src/app/services/relatorios.service";
import { categorias }        from "../../others/constantes";

@Component({selector: "app-relatorio", templateUrl: "./relatorio.component.html", styleUrls: ["./relatorio.component.scss"]}) export class RelatorioComponent implements OnInit {
  
    private itens:Item[] = [];
    private itensFiltrados:Item[] = [];
    private dataDe:Date;
    private dataAte:Date;
    private tag:number;
    private errMsg:string;
    private categor:string[] = categorias;

    constructor(private route:Router, private relatApi:RelatoriosService) { 
    }

    ngOnInit() {
        this.errMsg = "buscando dados ...";
        this.dataDe = new Date("01/01/2020");
        this.dataAte = new Date("12/31/2020");
        this.relatApi.filtrarPorData(this.dataDe, this.dataAte).toPromise().then(r => {
            if ((r as any).status) {
                var dados:Item[] = (r as any).data;
                for (var i:number = 0; i < dados.length; i++) {
                    dados[i].date = new Date(dados[i].date);
                    this.itens.push(dados[i]);
                }
                this.itensFiltrados = this.itens;
                this.errMsg = null;
            }
            else {
                this.snack("ERRO AO BUSCAR OS DADOS");
                console.log(JSON.stringify(r));
            }
        })
        .catch(e => {
            this.snack("ERRO AO BUSCAR OS DADOS");
            console.log(JSON.stringify(e));
        });
    }

    private filtrar():void {
        this.dataDe = new Date(this.dataDe);
        this.dataAte = new Date(this.dataAte);
        this.itensFiltrados = this.itens.filter(i => 
            (this.tag     ? i.tag  == this.tag     : true) && 
            (this.dataDe  ? i.date >= this.dataDe  : true) && 
            (this.dataAte ? i.date <= this.dataAte : true)
        );
    }

    private limpar():void {
        this.tag = this.dataDe = this.dataAte = null;
        this.itensFiltrados = this.itens;
    }

    private voltar():void {
        this.route.navigate(["/cadastrar"]);
    }

    private categDescrip(catId:number):string {
        return this.categor[catId];
    }

    private snack(str:string):void {
        this.errMsg = str;
        setTimeout(() => { this.errMsg = null; }, 3000);
    }

}
