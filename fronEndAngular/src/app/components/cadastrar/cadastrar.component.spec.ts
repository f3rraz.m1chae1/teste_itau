import { NO_ERRORS_SCHEMA } from "@angular/core";
import { CadastrarComponent } from "./cadastrar.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";

describe("CadastrarComponent", () => {

  let fixture: ComponentFixture<CadastrarComponent>;
  let component: CadastrarComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
      ],
      declarations: [CadastrarComponent]
    });

    fixture = TestBed.createComponent(CadastrarComponent);
    component = fixture.componentInstance;

  });

  it("should be able to create component instance", () => {
    expect(component).toBeDefined();
  });
  
});
