import { Component, OnInit } from "@angular/core";
import { Item, User }        from "src/app/models/models";
import { Router }            from "@angular/router";
import { CadastroService }   from "src/app/services/cadastro.service";
import { categorias }        from "../../others/constantes";

@Component({selector: "app-cadastrar", templateUrl: "./cadastrar.component.html", styleUrls: ["./cadastrar.component.scss"]}) export class CadastrarComponent implements OnInit {
  
    private model:Item = { id: 0, value: null, date: null };
    private usuario:User;
    private errMsg:string = null;
    private categs:string[] = categorias;

    constructor(private router:Router, private cadastroApi:CadastroService) { 
    }

    ngOnInit() {
        var userStr = localStorage.getItem("dadosUsuario");
        try {
            if (userStr) this.usuario = JSON.parse(userStr);
        }catch{}
    }

    private relat():void {
        this.router.navigate(["/relatorio"])
    }

    private limpar():void {
        this.model = { id: 0, value: null, date: null };
        this.errMsg = null;
    }

    private salvar():void {
        var str:string = null;
        if (!this.model.date)  str = "a data";
        if (!this.model.value) str = "o valor";
        if (str) {
            this.snack("é necessário inserir " + str);
            return;
        }
        this.errMsg = "SALVANDO ...";
        this.cadastroApi.criarItem(this.model).toPromise().then(r => {
            if ((r as any).status) this.snack("ITEM CADASTRADO COM SUCESSO"); 
            else {
                this.snack("ERRO AO SALVAR O ITEM");
                console.log(JSON.stringify(r));
            }
        }).catch(e => {
            this.snack("ERRO AO SALVAR O ITEM");
            console.log(JSON.stringify(e));
        });
    }

    private sair():void {
        this.router.navigate(["/login"]);
    }

    private snack(str:string):void {
        this.errMsg = str;
        setTimeout(() => { this.errMsg = null; }, 3000);
    }

}
