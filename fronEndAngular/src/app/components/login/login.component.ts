import { Component, OnInit } from "@angular/core";
import { Router }            from "@angular/router";
import { AuthenticationService } from "src/app/services/authentication.service";

@Component({selector: "app-login", templateUrl: "./login.component.html", styleUrls: ["./login.component.scss"]}) export class LoginComponent implements OnInit {
  
    private user:string = null;
    private pw:string = null;
    private errMsg:string = null;

    constructor(private router:Router, private authApi:AuthenticationService) { 
    }

    ngOnInit() {
    }

    private submit():void {
        var msg:string = null;
        if (!this.pw) msg = "preencha a senha";
        if (!this.user) msg = "preencha o usuário corretamente";
        if (msg) {
            this.errMsg = msg;
            setTimeout(() => { this.errMsg = null }, 3000);
            return;
        };
        this.errMsg = "CONECTANDO . . .";
        this.authApi.logar(this.user, this.pw).toPromise().then((r) => {
            localStorage.setItem("dadosUsuario", JSON.stringify((r as any).data));
            this.router.navigate(["cadastrar"]);
            this.errMsg = null;
        }).catch(e => {
            alert("Ocorreu um erro ao fazer o login");
            console.log("ERRO DE LOGIN:\n" + JSON.stringify(e));
            this.errMsg = null;
        });

        
    }

}
