import { RelatoriosService } from "./relatorios.service";
import { TestBed } from "@angular/core/testing";

describe("RelatoriosService", () => {

  let service: RelatoriosService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RelatoriosService
      ]
    });
    service = TestBed.get(RelatoriosService);

  });

  it("should be able to create service instance", () => {
    expect(service).toBeDefined();
  });

});
