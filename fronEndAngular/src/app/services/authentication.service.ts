import { Injectable }  from "@angular/core";
import { HttpClient }  from "@angular/common/http";
import { User }        from "../models/models";
import { environment } from "../../environments/environment";

var authApiUrl:string = environment.authApiUrl;

@Injectable() export class AuthenticationService {

    constructor(private http:HttpClient) {
    }

    public logar(user:string, pwHash:string) {
        var body:User = { id: 0, user: user, pwHash: pwHash, nickName: "" };
        return this.http.post(authApiUrl + "logar", body);
    }

}
