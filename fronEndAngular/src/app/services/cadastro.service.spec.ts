import { CadastroService } from "./cadastro.service";
import { TestBed } from "@angular/core/testing";

describe("CadastroService", () => {

  let service: CadastroService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CadastroService
      ]
    });
    service = TestBed.get(CadastroService);

  });

  it("should be able to create service instance", () => {
    expect(service).toBeDefined();
  });

});
