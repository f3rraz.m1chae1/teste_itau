import { Injectable }  from "@angular/core";
import { HttpClient }  from "@angular/common/http";
import { environment } from "../../environments/environment";

var relatApiUrl:string = environment.relatorioApiUrl;

@Injectable() export class RelatoriosService {

    constructor(private http:HttpClient) {
    }

    public filtrarPorData(dataI:Date, dataF:Date) {
        return this.http.get(relatApiUrl + "filtrarpordata?dataInicial=" + this.dateFormat(dataI) + "&dataFinal=" + this.dateFormat(dataF));
    }

    private dateFormat(value:Date):string {
        const dt:Date = value;
        const mm = dt.getMonth() + 1;
        const dd = dt.getDate();
        return dt.getFullYear() + "-" + (mm < 10 ? "0" + mm : mm) + "-" + (dd < 10 ? "0" + dd : dd);
    }

}
