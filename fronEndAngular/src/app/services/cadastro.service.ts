import { Injectable }  from "@angular/core";
import { HttpClient }  from "@angular/common/http";
import { Item }        from "../models/models";
import { environment } from "../../environments/environment";

var cadastroApiUrl:string = environment.cadastroApiUrl;

@Injectable() export class CadastroService {

    constructor(private http:HttpClient) {
    }

    public criarItem(item:Item) {
        return this.http.post(cadastroApiUrl + "criar", item);
    }

}
