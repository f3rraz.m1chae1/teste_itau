
export const environment = {
    production     : false, 
    authApiUrl     : "https://localhost:44322/api/User/v1/", 
    cadastroApiUrl : "https://localhost:44357/api/Item/v1/",
    relatorioApiUrl: "https://localhost:44383/api/Filtro/v1/"
};

